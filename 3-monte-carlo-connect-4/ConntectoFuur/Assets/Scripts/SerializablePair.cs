﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    [Serializable]
    public class SerializablePair<T1, T2, T3>
    {
        public T1 Value;
        public T2 Value2;
        public T2 Value3;
    }
}
