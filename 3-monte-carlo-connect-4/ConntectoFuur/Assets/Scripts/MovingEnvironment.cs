using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MovingEnvironment : MonoBehaviour
{

    [SerializeField] private List<SerializablePair<GameObject, float, float>> m_envoronmentParticles = new List<SerializablePair<GameObject, float, float>>();

    [SerializeField] private float m_effectsRandomnes = 0.2f;
    [SerializeField] private float m_startZ = 100f;
    private float m_fDeltaTime;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        m_fDeltaTime += Time.deltaTime;
        if (m_fDeltaTime > 0.5f)
        {
            m_fDeltaTime = 0;
            var number = Random.value;

            if (number < m_effectsRandomnes)
            {
                foreach (var effect in m_envoronmentParticles)
                {
                    // spawn effect
                    //var effect = m_envoronmentParticles.First();

                    // board mid point
                    // TODO: maybe use follow game object bc main camera is following it
                    float midY = (ConnectFourBoardGenerator.FRAME_PIECE_SIZE * ConnectFourBoardGenerator.BOARD_SIZE) / 2 - (ConnectFourBoardGenerator.FRAME_PIECE_SIZE / 2);

                    var y = (Random.value * (effect.Value3 * 2)) - effect.Value3;
                    var x = 2.5f + (Random.value * effect.Value2);

                    // spawn right side
                    if (Random.value > 0.5)
                    {
                        var go = Instantiate(effect.Value, this.transform);
                        go.transform.position = new Vector3(((ConnectFourBoardGenerator.BOARD_SIZE / 2) * ConnectFourBoardGenerator.FRAME_PIECE_SIZE) + x, midY + y, m_startZ);
                    }

                    // spawn left side
                    if (Random.value < 0.5)
                    {
                        var go = Instantiate(effect.Value, this.transform);
                        go.transform.position = new Vector3(-((ConnectFourBoardGenerator.BOARD_SIZE / 2) * ConnectFourBoardGenerator.FRAME_PIECE_SIZE) - x, midY + y, m_startZ);
                    }
                }

            }
        }
    }
}
