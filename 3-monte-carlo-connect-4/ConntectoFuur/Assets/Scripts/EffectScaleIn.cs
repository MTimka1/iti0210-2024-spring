using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectScaleIn : MonoBehaviour
{

    [SerializeField] private float m_speed = 0.5f;

    private float m_fLerpVal = 0;
    private Vector3 m_scaleTo;
    private bool m_bFinished;

    // Start is called before the first frame update
    void Start()
    {
        this.m_scaleTo = transform.localScale;
        transform.localScale = new Vector3(0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {

        if (m_bFinished == false)
        {
            m_fLerpVal += Time.deltaTime * m_speed;
            transform.localScale = Vector3.Lerp(Vector3.zero, m_scaleTo, m_fLerpVal);

            if (m_fLerpVal > 1.0f)
            {
                m_bFinished = true;
            }
        }
    }
}
