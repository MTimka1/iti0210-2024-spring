using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayRandomAnimationOn : MonoBehaviour
{
    [SerializeField] private Animator m_Animator;
    [SerializeField] private List<string> m_animationTriggers = new List<string>();

    public void PlayAnimation(ConnectFourBoardGenerator script)
    {
        if (m_animationTriggers.Count > 0)
        {
            var ri = Random.Range(0, (m_animationTriggers.Count - 1));
            m_Animator.SetTrigger(m_animationTriggers[ri]);

        }
    }
}
