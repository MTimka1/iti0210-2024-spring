using Assets.Scripts;
using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

public class PieceScript : MonoBehaviour
{

    [SerializeField] public float m_speed = 0.8f;
    [SerializeField] public float gravity = 1f;

    [Space]
    [SerializeField] private List<SerializablePair<PlayerSide, Material, Material>> m_playerMaterials = new List<SerializablePair<PlayerSide, Material, Material>>();

    public Point Location { get; private set; }
    public PlayerSide Side { get; private set; }


    private float m_fDropToLocalY = 0;
    private bool m_bCompletedDrop = true;
    [CanBeNull] private Action<Point, PlayerSide, PieceScript> m_aOnCompleted = null;
    private bool m_bEventFired = false;
    private float velocityY = 0f;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (m_bCompletedDrop == false)
        {
            velocityY += (gravity * Time.deltaTime) * m_speed;

            var targetY = m_fDropToLocalY + this.transform.parent.position.y;
            float newY = Mathf.Lerp(transform.position.y, targetY, velocityY);
            transform.position = new Vector3(transform.position.x, newY, transform.position.z);

            if (Mathf.Approximately(transform.position.y, targetY))
            {
                m_bCompletedDrop = true;
                velocityY = 0f;
            }

            if (m_bEventFired == false)
            {
                if (Mathf.Abs(transform.position.y - targetY) < 0.1f)
                {
                    m_aOnCompleted?.Invoke(Location, Side, this);
                    m_bEventFired = true;
                }
            }
        }

    }

    [CanBeNull]
    private Material? GetPlayerMaterial(PlayerSide side)
    {
        foreach (var pm in m_playerMaterials)
        {
            if (pm.Value == side)
            {
                return pm.Value2;
            }
        }

        Debug.LogError("PieceScript cannot find material for player " + Enum.GetName(typeof(PlayerSide), side));

        return null;
    }

    [CanBeNull]
    private Material? GetPlayerWinMaterial(PlayerSide side)
    {
        foreach (var pm in m_playerMaterials)
        {
            if (pm.Value == side)
            {
                return pm.Value3;
            }
        }

        Debug.LogError("PieceScript cannot find win material for player " + Enum.GetName(typeof(PlayerSide), side));

        return null;
    }

    /// <summary>
    /// how many steps to drop from top to down
    /// </summary>
    /// <param name="dropToLocalY"></param>
    public void DropTo(Point location, PlayerSide side, Action<Point, PlayerSide, PieceScript> onCompleted)
    {
        this.Location = location;
        this.Side = side;

        var xmod = (ConnectFourBoardGenerator.FRAME_PIECE_SIZE * ConnectFourBoardGenerator.BOARD_SIZE) / 2 - (ConnectFourBoardGenerator.FRAME_PIECE_SIZE / 2);

        var m_fPieceDropY = ((ConnectFourBoardGenerator.BOARD_SIZE) * ConnectFourBoardGenerator.FRAME_PIECE_SIZE);
        this.transform.position = new Vector3(location.X * ConnectFourBoardGenerator.FRAME_PIECE_SIZE - xmod, m_fPieceDropY, 0) + this.transform.position; // edit local position
        this.GetComponent<MeshRenderer>().material = GetPlayerMaterial(side);

        //m_fDropToLocalY = dropToLocalY; // reverse the position
        m_fDropToLocalY = (location.Y * ConnectFourBoardGenerator.FRAME_PIECE_SIZE);
        m_aOnCompleted = onCompleted;

        m_bEventFired = false;
        m_bCompletedDrop = false;
    }

    public void LightUp()
    {
        this.GetComponent<MeshRenderer>().material = GetPlayerWinMaterial(Side);
    }
}
