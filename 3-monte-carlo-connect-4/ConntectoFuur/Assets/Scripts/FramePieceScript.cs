using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FramePieceScript : MonoBehaviour
{
    [CanBeNull]
    private PieceScript? m_piece = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnDestroy()
    {
        if (m_piece != null)
        {
            Destroy(m_piece.gameObject);
        }
    }

    public void SetPiece(PieceScript script)
    {
        this.m_piece = script;
    }

    [CanBeNull]
    public PieceScript GetPiece()
    {
        return this.m_piece;
    }
}
