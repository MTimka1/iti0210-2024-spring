using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObjectToPoint : MonoBehaviour
{

    [SerializeField] private float m_baseSpeed = 2.0f;
    [SerializeField] private Vector3 m_moveTo = Vector3.zero;


    [Space]
    [SerializeField] private bool m_moveZ = true;

    private Vector3 m_startPosition;


    // Start is called before the first frame update
    void Start()
    {
        m_startPosition = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        var step = (Time.deltaTime * m_baseSpeed);

        if (m_moveZ == true)
        {
            if (m_moveTo.z - m_startPosition.z > 0)
            {
                if (this.transform.position.z + step > m_moveTo.z)
                {
                    this.transform.position = new Vector3(0, 0, m_moveTo.z);
                    Destroy(this.gameObject);
                }
                else
                {
                    this.transform.position += new Vector3(0, 0, step);
                }
            }
            else
            {
                step *= -1;
                if (this.transform.position.z + step < m_moveTo.z)
                {
                    this.transform.position = new Vector3(0, 0, m_moveTo.z);
                    Destroy(this.gameObject);
                }
                else
                {
                    this.transform.position += new Vector3(0, 0, step);
                }
            }
        }
    }
}
