using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeToLive : MonoBehaviour
{
    [SerializeField] private float m_timeToLive = 8.0f;

    private float m_fDeltaTime = 0;

    // Start is called before the first frame update
    void Start()
    {
        m_fDeltaTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        m_fDeltaTime += Time.deltaTime;
        if (m_fDeltaTime > m_timeToLive)
        {
            Destroy(this.gameObject);
        }
    }
}
