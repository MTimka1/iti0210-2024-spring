﻿using PimDeWitte.UnityMainThreadDispatcher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public class Log
    {
        public static void d(string log)
        {
            UnityEngine.Debug.Log(log);
            try
            {
                UnityMainThreadDispatcher.Instance().Enqueue(() => {
                    TIM.Console.Log(log);
                });
            }
            catch (Exception e) 
            {
                UnityEngine.Debug.LogError(e.Message);
            }
        }
    }
}
