using Assets.Scripts;
using Cinemachine;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using PimDeWitte.UnityMainThreadDispatcher;
using Newtonsoft.Json.Linq;

public class ConnectFourBoardGenerator : MonoBehaviour
{
    public static float FRAME_PIECE_SIZE = 0.4f;
    public static float BOARD_SIZE = 0;

    /// <summary>
    /// List<X<Y>>, x[y[]y[]] x[y[]y[]]
    /// </summary>
    public static List<List<PlayerSide>> BOARD_STATE = new List<List<PlayerSide>>();


    [SerializeField] private GameObject m_centerPiece;
    [SerializeField] private GameObject m_bottomMiddlePiece;
    [SerializeField] private GameObject m_piecePrefab;
    [SerializeField] private Transform m_pieces;
    [SerializeField] private Transform m_follow;
    [SerializeField] private int m_boardSize = 6;
    [SerializeField] private int m_connectCount = 4;
    [SerializeField] private PlayerSide m_aiPlayer;

    [Space]
    [SerializeField] private CinemachineFreeLook m_freelookCamera;

    [Space]
    [SerializeField] private Material m_selectedRowMaterial;
    [SerializeField] private Material m_normalMaterial;
    [SerializeField] private Material m_winMaterial;

    [SerializeField] private List<UnityEvent<ConnectFourBoardGenerator>> m_winActions = new List<UnityEvent<ConnectFourBoardGenerator>>();
    [SerializeField] private List<UnityEvent<ConnectFourBoardGenerator>> m_drawActions = new List<UnityEvent<ConnectFourBoardGenerator>>();

    public List<List<GameObject>> m_framePieces = new List<List<GameObject>>();

    [DisableInEditorMode]
    [DisableInPlayMode]
    public int SelectedRow = 0;

    private int lastSelectedRow = 0;
    private PlayerSide m_selectedPlayer = PlayerSide.NONE;

    private bool m_bPieceIsDropping = false;
    private bool m_gameEnded = false;

    private StreamWriter streamWriter;
    private StreamReader streamReader;


    void Start()
    {
        BOARD_SIZE = m_boardSize;
        Task.Run(RunBackgroundEngine);
    }

    private void Update()
    {
        if (m_gameEnded == false)
        {
            // make the row lit up which is selected
            {
                // check which row is selected be camera
                var range = m_freelookCamera.m_XAxis.m_MaxValue - m_freelookCamera.m_XAxis.m_MinValue;
                var rowRange = Convert.ToInt32(range / m_boardSize);

                SelectedRow = Convert.ToInt32(m_freelookCamera.m_XAxis.Value - m_freelookCamera.m_XAxis.m_MinValue) / rowRange;
                if (SelectedRow >= m_boardSize) // make sure we are not gowing over possible selections
                { SelectedRow = m_boardSize - 1; }

                SelectedRow = (m_boardSize - 1) - SelectedRow; // reverse

                if (lastSelectedRow != SelectedRow)
                {
                    // restore last selected pieces material to normal
                    LightDownRow(lastSelectedRow);

                    lastSelectedRow = SelectedRow;

                    // change pieces material to selected one
                    LightUpRow(SelectedRow);
                }
            }

            // drop the piece in selected row
            {
                if (m_bPieceIsDropping == false)
                {
                    if (Input.GetKeyDown(KeyCode.Space))
                    {
                        m_bPieceIsDropping = true;
                        Task.Run(DropPieceToSelectedRow);
                    }
                }
            }
        }

    }

    // ====================== Background Engine Tasks

    private string ReadTilCanEnterCommand()
    {

        string output = "";
        while (true)
        {
            var c = Convert.ToChar(streamReader.Read());
            output += c;

            if (output.EndsWith("# ") || c == 0)
            {
                break;
            }
        }

        return output;
    }

    private void RunBackgroundEngine()
    {
        Process proc = new Process();
        proc.StartInfo.FileName = "python";
        //proc.StartInfo.Arguments = "F:/UnityProjects/ConntectoFuur/BackgroundEngine/BackgroundEngine.py";
        proc.StartInfo.Arguments = "BackgroundEngine/BackgroundEngine.py";
        //proc.StartInfo.Arguments = "BackgroundEngine/BackgroundEngine.py";
        proc.StartInfo.UseShellExecute = false;
        proc.StartInfo.RedirectStandardInput = true;
        proc.StartInfo.RedirectStandardOutput = true;
        proc.StartInfo.CreateNoWindow = true;
        //proc.StartInfo.CreateNoWindow = false;
        proc.Start();

        streamWriter = proc.StandardInput;
        streamReader = proc.StandardOutput;


        var output = ReadTilCanEnterCommand();
        Log.d("BackgroundEngine: " + output);

        streamWriter.WriteLine("set board_size " + m_boardSize);
        output = ReadTilCanEnterCommand();
        Log.d("BackgroundEngine: " + output);

        streamWriter.WriteLine("set connect_count " + m_connectCount);
        output = ReadTilCanEnterCommand();
        Log.d("BackgroundEngine: " + output);

        UnityMainThreadDispatcher.Instance().Enqueue(() => {
            RestartBoard();
        });
    }

    private void DropPieceToSelectedRow()
    {
        string playerId = GetPlayerIdString(m_selectedPlayer);

        streamWriter.WriteLine($"play {playerId} {SelectedRow}");
        var output = ReadTilCanEnterCommand();
        Log.d("BackgroundEngine: " + output);

        if (output.Contains("dropped piece to "))
        {
            var y = int.Parse(output.Split("Y:")[1].Split("\n")[0]);
            UnityMainThreadDispatcher.Instance().Enqueue(() => {
                var go = Instantiate(m_piecePrefab, m_pieces);
                go.GetComponent<PieceScript>().DropTo(new Point(SelectedRow, y), m_selectedPlayer, OnPieceDropped);
            });
        }
        else
        {
            m_bPieceIsDropping = false;
        }

    }





    // will be called from Update so do not make it compute heavy
    private void OnPieceDropped(Point location, PlayerSide side, PieceScript script)
    {
        BOARD_STATE[location.X][location.Y] = side;
        m_framePieces[location.X][location.Y].GetComponent<FramePieceScript>().SetPiece(script);


        // change player
        if (m_selectedPlayer == PlayerSide.PLAYER_1)
        { m_selectedPlayer = PlayerSide.PLAYER_2; }
        else if (m_selectedPlayer == PlayerSide.PLAYER_2)
        { m_selectedPlayer = PlayerSide.PLAYER_1; }

        CheckForWin(_false: () =>
        {
            IsBoardFull(_false: () =>
            {
                // if current player is ai then think move for it
                if (m_selectedPlayer == m_aiPlayer)
                {
                    ThinkForAi();
                }
            });

        });
    }

    private string GetPlayerIdString(PlayerSide player)
    {
        if (player == PlayerSide.PLAYER_1)
        { return "player1"; }
        else if (player == PlayerSide.PLAYER_2)
        { return "player2"; }
        else { return ""; }
    }

    private void IsBoardFull(Action? _false)
    {
        Task.Run(() =>
        {
            streamWriter.WriteLine("is board full");
            var output = ReadTilCanEnterCommand();
            Log.d("BackgroundEngine: " + output);

            if (output.Contains("True"))
            {
                Log.d("Game Draw");

                foreach (var action in m_drawActions)
                {
                    action.Invoke(this);
                }
            }
            else
            {
                _false?.Invoke();
            }
        });
    }

    private void ThinkForAi()
    {
        Task.Run(() =>
        {
            streamWriter.WriteLine("think " + GetPlayerIdString(m_selectedPlayer));
            var output = ReadTilCanEnterCommand();
            Log.d("BackgroundEngine: " + output);

            if (output.Contains("Cannot Think Anymore"))
            {
                // TODO: maybe draw
                foreach (var action in m_drawActions)
                {
                    action.Invoke(this);
                }
            }
            else
            {
                var x = int.Parse(output.Split("X:")[1].Split(" ")[0]);
                var y = int.Parse(output.Split("Y:")[1].Split("\n")[0]);

                UnityMainThreadDispatcher.Instance().Enqueue(() => {
                    var go = Instantiate(m_piecePrefab, m_pieces);
                    go.GetComponent<PieceScript>().DropTo(new Point(x, y), m_aiPlayer, OnPieceDropped);
                });
            }
        });
    }

    private void CheckForWin(Action? _false = null)
    {
        Task.Run(() =>
        {
            streamWriter.WriteLine("check for win");
            var output = ReadTilCanEnterCommand();
            Log.d("BackgroundEngine: " + output);

            var o = JObject.Parse(output.Split("# ")[0]);
            var status = o.GetValue("status").ToObject<bool>();

            if (status == true)
            {
                UnityMainThreadDispatcher.Instance().Enqueue(() => {
                    m_gameEnded = true;

                    LightDownRow(SelectedRow);

                    foreach (var action in m_winActions)
                    {
                        action.Invoke(this);
                        LightUpWinPieces(o.GetValue("pieces").ToObject<List<List<int>>>());
                    }
                });
            }
            else
            {
                UnityMainThreadDispatcher.Instance().Enqueue(() => {
                    // if current player is not ai then we can move
                    if (m_selectedPlayer != m_aiPlayer)
                    {
                        m_bPieceIsDropping = false;
                    }
                });

                _false?.Invoke();
            }
        });

    }

    private void LightUpWinPieces(List<List<int>> list)
    {
        foreach (var item in list)
        {
            m_framePieces[item[0]][item[1]].GetComponent<FramePieceScript>().GetPiece().LightUp();
        }
    }

    private void GenerateConnectFourBoard()
    {

        foreach (var x in m_framePieces)
        {
            foreach (var y in x)
            {
                Destroy(y);
            }
        }
        m_framePieces.Clear();
        BOARD_STATE.Clear();

        for (int x = 0; x < m_boardSize; x++)
        {
            var column = new List<GameObject>();
            var stateColumn = new List<PlayerSide>();

            var xmod = (FRAME_PIECE_SIZE * m_boardSize) / 2 - (FRAME_PIECE_SIZE / 2);

            var goX = Instantiate(m_bottomMiddlePiece, this.transform);
            goX.transform.position = this.transform.position + new Vector3(x * FRAME_PIECE_SIZE - xmod, 0, 0);
            column.Add(goX);
            stateColumn.Add(PlayerSide.NONE);

            for (int y = 1; y < m_boardSize; y++)
            {
                var goY = Instantiate(m_centerPiece, this.transform);
                goY.transform.position = this.transform.position + new Vector3(x * FRAME_PIECE_SIZE - xmod, y * FRAME_PIECE_SIZE, 0);
                column.Add(goY);
                stateColumn.Add(PlayerSide.NONE);
            }

            m_framePieces.Add(column);
            BOARD_STATE.Add(stateColumn);
        }

        //m_follow.position = new Vector3((FRAME_PIECE_SIZE * m_boardSize) / 2 - 0.2f, (FRAME_PIECE_SIZE * m_boardSize) / 2 - (FRAME_PIECE_SIZE/2), 0);
        m_follow.position = new Vector3(0, (FRAME_PIECE_SIZE * m_boardSize) / 2 - (FRAME_PIECE_SIZE/2), 0);
        // move every piece to left by (FRAME_PIECE_SIZE * m_boardSize) / 2 - 0.2f
    }

    private void LightUpRow(int row)
    {
        for (int x = 0; x < m_framePieces.Count; x++)
        {
            if (x == row)
            {
                foreach (var y in m_framePieces[x])
                {
                    y.GetComponent<MeshRenderer>().material = m_selectedRowMaterial;
                }
            }
        }
    }

    private void LightDownRow(int row)
    {
        for (int x = 0; x < m_framePieces.Count; x++)
        {
            if (x == row)
            {
                foreach (var y in m_framePieces[x])
                {
                    y.GetComponent<MeshRenderer>().material = m_normalMaterial;
                }
            }
        }
    }

    private void LightUpWinPattern(Point winPoint, WinPattern winPattern)
    {
        if (winPattern == WinPattern.VECRTICAL)
        {
            for (int i = 0; i < m_connectCount; i++)
            {
                //m_framePieces[winPoint.X][i].GetComponent<MeshRenderer>().material = m_winMaterial;
                m_framePieces[winPoint.X][winPoint.Y+i].GetComponent<FramePieceScript>().GetPiece().LightUp();
            }
        }

        if (winPattern == WinPattern.HORTIZONTAL)
        {
            for (int i = 0; i < m_connectCount; i++)
            {
                //m_framePieces[i][winPoint.Y].GetComponent<MeshRenderer>().material = m_winMaterial;
                m_framePieces[winPoint.X+i][winPoint.Y].GetComponent<FramePieceScript>().GetPiece().LightUp();
            }
        }

        if (winPattern == WinPattern.DIAGONAL_LEFT_BOTTOM_TO_RIGHT_UP)
        {
            for (int i = 0; i < m_connectCount; i++)
            {
                //m_framePieces[winPoint.X+i][winPoint.Y+i].GetComponent<MeshRenderer>().material = m_winMaterial;
                m_framePieces[winPoint.X+i][winPoint.Y+i].GetComponent<FramePieceScript>().GetPiece().LightUp();
            }
        }

        if (winPattern == WinPattern.DIAGONAL_LEFT_TOP_TO_RIGHT_BOTTOM)
        {
            for (int i = 0; i < m_connectCount; i++)
            {
                //m_framePieces[winPoint.X+i][winPoint.Y-i].GetComponent<MeshRenderer>().material = m_winMaterial;
                m_framePieces[winPoint.X+i][winPoint.Y-i].GetComponent<FramePieceScript>().GetPiece().LightUp();
            }
        }
    }

    // will be called from unity event from modal window
    public void RestartBoard()
    {
        Task.Run(() =>
        {
            streamWriter.WriteLine("clear state");
            var output = ReadTilCanEnterCommand();
            Log.d("BackgroundEngine: " + output);

            if (output.Contains("State Cleared"))
            {
                UnityMainThreadDispatcher.Instance().Enqueue(() => {
                    GenerateConnectFourBoard();
                    m_selectedPlayer = PlayerSide.PLAYER_1;
                    m_gameEnded = false;
                    m_bPieceIsDropping = false;
                });
            }
        });
    }
}
