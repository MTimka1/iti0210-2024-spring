using Michsky.MUIP;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowModal : MonoBehaviour
{
    [SerializeField] private ModalWindowManager m_modal;
    [SerializeField] private float m_delay = 3.0f;

    private float m_fDeltaTime = 0;
    private bool m_bStartShowingModal = false;

    private void Update()
    {
        if (m_bStartShowingModal == true)
        {
            m_fDeltaTime += Time.deltaTime;
            if (m_fDeltaTime > m_delay)
            {
                m_bStartShowingModal = false;
                m_modal.Open();
            }
        }
    }

    public void Show(ConnectFourBoardGenerator script)
    {
        m_fDeltaTime = 0;
        m_bStartShowingModal = true;
    }
}
