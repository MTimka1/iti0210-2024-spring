﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public enum PlayerSide
    {
        NONE, PLAYER_1, PLAYER_2
    }
}
