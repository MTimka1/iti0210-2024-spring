using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EffectsDrawer : MonoBehaviour
{

    [SerializeField] private ConnectFourBoardGenerator m_boardGenerator;
    [SerializeField] private float m_effectsRandomnes = 0.2f;

    [Space]
    [SerializeField] private List<GameObject> m_effects = new List<GameObject>();

    private float m_fDeltaTime;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        m_fDeltaTime += Time.deltaTime;
        if (m_fDeltaTime > 1.0f)
        {
            m_fDeltaTime = 0;
            var number = Random.value;
            
            if (number < m_effectsRandomnes)
            {
                // spawn effect
                var effect = m_effects.First();

                // spawn right side
                {
                    var go = Instantiate(effect, this.transform);
                    go.transform.position = new Vector3(((ConnectFourBoardGenerator.BOARD_SIZE / 2) * ConnectFourBoardGenerator.FRAME_PIECE_SIZE) + 2.5f, go.transform.position.y, 0);
                }

                // spawn left side
                {
                    var go = Instantiate(effect, this.transform);
                    go.transform.position = new Vector3(-((ConnectFourBoardGenerator.BOARD_SIZE / 2) * ConnectFourBoardGenerator.FRAME_PIECE_SIZE) - 2.5f, go.transform.position.y, 0);
                }

            }
        }
    }
}
