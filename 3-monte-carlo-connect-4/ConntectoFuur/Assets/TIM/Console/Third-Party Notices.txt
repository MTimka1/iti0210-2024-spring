1. Asset uses the font "M PLUS Rounded 1c" under Open Font Licence.
	- Font:https://fonts.google.com/specimen/M+PLUS+Rounded+1c/about
	- Licence: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
	
2. Asset uses the font "NotoSansSC-Regular" under "SIL Open Font License 1.1"
	
3. Asset built on top of "Odin Inspector and Serializer" functionality. You must have this asset imported.
    - Asset Store page: https://assetstore.unity.com/packages/tools/utilities/odin-inspector-and-serializer-89041
	
4. All other content is original and free to use.