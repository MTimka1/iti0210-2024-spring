import json
import random
import numpy as np
import ast
import traceback


class ConnectFour:
    def __init__(self, board_size, connect_count, player_ids):
        self.player_ids = player_ids
        self.board_size = board_size
        self.connect_count = connect_count
        self.board_state = []
        # generate baord 2d vector
        for x in range(board_size):
            ys = []
            for y in range(board_size):
                ys.append(None) # set nothing
            self.board_state.append(ys)
        
        # create new game .txt file
        self.game_filename = "game.txt"
        with open(self.game_filename, 'w') as file:
            file.write(f"Game {board_size}x{board_size} with {player_ids} to connect {connect_count}\n")
            file.write(f"Lowest score is best\n")
            file.write(f"x in the ascii board means next placement on the piece\n\n")
    
    def think(self, player_id_to_think_for):
        if player_id_to_think_for not in self.player_ids:
            return None
    
        # to save time we are going to use current game to simulate further and later reset state to original
        # python can be very shitty with its auto pointers, so we have to stringify object and later deserialize
        orig_state = str(self.board_state)
        
        # check every x pos for n time the _ depth
        n_max = 10
        depth_max = 50
        x_scores = []
        got_any_move = False
        
        # get how those xes we can not drop to
        _filled_xes = []
        for x in range(self.board_size):
            if self.get_free_y_to_drop(x) == None:
                _filled_xes.append(x)
        #print("_filled_xes", _filled_xes)
        
        for x in range(self.board_size):
            score_sum = 0
            
            # reset state to simulate again
            self.board_state = ast.literal_eval(orig_state)
            y = self.get_free_y_to_drop(x)
            if y == None:
                x_scores.append(depth_max)
                continue
            
            # check if enemy would win if he/she placed piece there
            for player_id in self.player_ids:
                if player_id != player_id_to_think_for:
                    self.set_piece(x, y, player_id)
                    check, pid, pieces = self.check_for_win()
                    #print(f"{player_id} {pid} {check} {y}")
                    if check == True:
                        score_sum = 1
                        got_any_move = True
                        break
            
            # if previous have not modified the score_sum
            # if so then it must have urgent reason
            if score_sum == 0:
                for n in range(n_max):
                    score = depth_max
                    
                    # reset state to simulate again
                    self.board_state = ast.literal_eval(orig_state)
                    
                    filled_x = ast.literal_eval(str(_filled_xes))
                    available_xes = [item for item in range(self.board_size) if item not in filled_x]
                    
                    for _ in range(0, depth_max):
                        # NOTICE: added if sentence to complete the logic 1st step
                        if _ == 0:
                            # do not change x
                            _x = x
                            pass
                        else:
                            _x = random.choice(available_xes)
                            
                        y = self.get_free_y_to_drop(_x)
                        
                        if y == None:
                            filled_x.append(_x)
                            available_xes = [item for item in available_xes if item not in filled_x]
                            
                            if len(available_xes) == 0:
                                break
                        else:
                            # NOTICE: changed also to meet the logic
                            # ever non mod 2 will place bot
                            play_player_id = player_id_to_think_for
                            if _ % 2 == 0:
                                play_player_id = [item for item in self.player_ids if item != player_id_to_think_for][0]
                        
                            self.set_piece(_x, y, play_player_id)
                            check, player_id, pieces = self.check_for_win()
                            if check == True:
                                if player_id_to_think_for == player_id:
                                    score = _ # set how many septs we took to win
                                    got_any_move = True
                                else:
                                    score = depth_max # give worst score if enemy will win
                                break
                    score_sum += score
                    
                    # if the current x is winning placement then ignore rest of checking on that x
                    if score == 0:
                        break
            x_scores.append(score_sum / n_max) # add average score of dis x
            
        # reset state to original game
        self.board_state = ast.literal_eval(orig_state)
        
        #print("scores", x_scores)
        #print("orig", orig_state)
        
        
        # if we havent found any move then 
        if got_any_move == False:
            # check if can place piece
            if len(_filled_xes) >= self.board_size:
                return None
            else:
                #print("Get Random since " + str(_filled_xes))
                # choose random move if we have not found any gud move
                available_xes = [item for item in range(self.board_size) if item not in _filled_xes]
                random_x = random.choice(available_xes)
                return random_x
        
        best_x = np.argmin(x_scores)
        self.write_file(self.board_state, x_scores, best_x)
        
        return best_x
    
    def write_file(self, board_state, x_scores, best_x):
        best_y = None
        for y in range(self.board_size):
            if board_state[best_x][y] == None:
                best_y = y
                break
                
        str_board = ""
        for y in range(self.board_size-1, -1, -1):
            for x in range(self.board_size):
                if y == best_y and x == best_x:
                    # show the next placement of piece
                    str_board += "[x] "
                elif board_state[x][y] != None:
                    # print the last char of player id hopefully it is 0, 1 or 2 from string player1 or player2
                    str_board += f"[{board_state[x][y][-1]}] "
                else:
                    str_board += "[ ] "
            str_board += "\n"
        
        if best_y != None:
            with open(self.game_filename, 'a') as file:
                file.write(str_board+"\n")
                file.write(str(x_scores)+"\n\n")
        else:
            with open(self.game_filename, 'a') as file:
                file.write(str_board+"\n")
                file.write(f"Cannot place on X:{best_x} Y:{best_y}\n")
                file.write("x_score: "+str(x_scores)+"\n\n")
                
    def set_piece(self, x: int, y: int, player_id):
        self.board_state[x][y] = player_id
    
    def get_free_y_to_drop(self, x: int):
        for y in range(self.board_size):
            if self.board_state[x][y] == None:
                return y
        return None;
    
    def is_board_full(self):
        # check if any top level has empty place
        for x in range(self.board_size):
            if self.board_state[x][self.board_size-1] == None:
                return False
        return True
    
    def check_for_win(self):
        for x in range(self.board_size): 
            for y in range(self.board_size):
                if self.board_state[x][y] == None:
                    continue
            
                # check for verticals
                connected_pieces = [[x, y]] # store to layer return to show on ui
                i = 1
                while i < self.connect_count:
                    if y+i >= self.board_size or self.board_state[x][y] != self.board_state[x][y+i]: break
                    connected_pieces.append([x, y+i])
                    i += 1
                if i == self.connect_count:
                    return True, self.board_state[x][y], connected_pieces # return that someone has win and its id, and pieces connected
                
                # check for horizontal
                connected_pieces = [[x, y]] # store to layer return to show on ui
                i = 1
                while i < self.connect_count:
                    if x+i >= self.board_size or self.board_state[x][y] != self.board_state[x+i][y]: break
                    connected_pieces.append([x+i, y])
                    i += 1
                if i == self.connect_count:
                    return True, self.board_state[x][y], connected_pieces # return that someone has win and its id, and pieces connected
                
                # check for diagonal left down ->(to) up right
                connected_pieces = [[x, y]] # store to layer return to show on ui
                i = 1
                while i < self.connect_count:
                    if x+i >= self.board_size or y+i >= self.board_size or self.board_state[x][y] != self.board_state[x+i][y+i]: break
                    connected_pieces.append([x+i, y+i])
                    i += 1
                if i == self.connect_count:
                    return True, self.board_state[x][y], connected_pieces # return that someone has win and its id, and pieces connected
                
                # check for diagonal left up ->(to) down right
                connected_pieces = [[x, y]] # store to layer return to show on ui
                i = 1
                while i < self.connect_count:
                    if x+i >= self.board_size or y-i < 0 or self.board_state[x][y] != self.board_state[x+i][y-i]: break
                    connected_pieces.append([x+i, y-i])
                    i += 1
                if i == self.connect_count:
                    return True, self.board_state[x][y], connected_pieces # return that someone has win and its id, and pieces connected
                    
        return False, None, None



"""
board develops from left bottom to right up
store board state as json, "player" > [[x1, y1], [x2, y2]]
{
    "player1": [
        [1, 2]
        [1, 3]
        [1, 4]
    ],
    "player2": [
        [0, 0],
        [1, 0],
    ]
}
"""
start_state = {"player1": [], "player2": []}
board_state = dict(start_state)
board_size = 4
connect_count = 4
game = None


def init_game():
    global game, board_size, connect_count, start_state
    game = ConnectFour(board_size, connect_count, board_state.keys())


""" old and slow
def get_free_y_to_drop(x: int):
    if x >= board_size:
        return None

    highest_y = -1
    
    # get the higest y on places x
    for player in board_state:
        for it in board_state[player]:
            if it[0] == x and it[1] > highest_y:
                highest_y = it[1]
    
    if highest_y < board_size:
        return highest_y + 1
        
    return None
"""


def process_input(command):
    global board_state, board_size, start_state, game
    
    try:
        if command.startswith("set "):
            cmd_pieces = command.split("set ")[1].split(" ")
            globals()[cmd_pieces[0]] = int(cmd_pieces[1])
            
            return f"{cmd_pieces[0]} = {globals()[cmd_pieces[0]]}"
            
        if command.startswith("get "):
            cmd_pieces = command.split("get ")[1].split(" ")
            if cmd_pieces[0] == "state":
                return json.dumps(board_state)
            else:
                return f"{cmd_pieces[0]} = {globals()[cmd_pieces[0]]}"
                
        if command == "clear state":
            # seems we cannot not copy start_state value, python is forcing to copy only its pointer T_T
            board_state = {"player1": [], "player2": []}
            init_game()
            return "State Cleared to: " + json.dumps(board_state)
            
        if command == "init game":
            init_game()
            return "Game Initialized"
            
        if command.startswith("play "):
            if game == None:
                return "Game is not Initialized"
        
            cmd_pieces = command.split("play ")[1].split(" ")
            player_id = cmd_pieces[0]
            x = int(cmd_pieces[-1])
            # y = get_free_y_to_drop(x)
            y = game.get_free_y_to_drop(x)
            
            if y == None:
                return f"Cannot drop piece {player_id} to X:{x}, board size is {board_size}"
            
            board_state[player_id].append([x, y])
            game.set_piece(x, y, player_id)
            return f"Player {player_id} dropped piece to X:{x} Y:{y}"
            
        if command == "check for win":
            if game == None:
                return "Game is not Initialized"
                    
            check, player_id, pieces = game.check_for_win()
            return json.dumps({
                "status": check,
                "player_id": player_id,
                "pieces": pieces
            })
            
        if command.startswith("think "):
            if game == None:
                return "Game is not Initialized"
                
            cmd_pieces = command.split("think ")[1].split(" ")
            player_id = cmd_pieces[0]
            
            x = game.think(player_id)
            
            if x == None:
                return f"Cannot Think for Player " + str(player_id)
                
            y = game.get_free_y_to_drop(x)
            
            if y == None:
                return f"Cannot Think Anymore"
                
            board_state[player_id].append([x, y])
            game.set_piece(x, y, player_id)
            return f"{player_id} think X:{x} Y:{y}"
            
        if command == "is board full":
            if game == None:
                return "Game is not Initialized"
                
            return str(game.is_board_full())
        
    except Exception as e:
        print(repr(e))
        print(traceback.format_exc())
    
    return "Unknown command: " + command

def main():
    print(
    """
    ConnectoFuur Background Engine v0.1
    Commands:
        set board_size 6       - set board size to 6x6
        set connect_count 4    - set connect_count to 4 (the needed amount to connect in order to win)
        get state              - return current state in json format
        get board_size         - return board_size variable
        get connect_count      - return connect_count variable
        clear state            - reset game, will also init game
        init game              - initialize game
        play player1 0         - drop player1 piece to X:0
        play player2 2         - drop player2 piece to X:2
        check for win          - check if someone has won the game, return won id and pieces connected
        think player2          - program will think move for player2
        is board full          - check if the board is full
        exit                   - exit program
    """
    )
    while True:
        try:
            print("# ", end='')
            user_input = " ".join(input().split())
            
            if not user_input:
                continue
            
            if user_input == "exit":
                break
            
            # Process the input
            result = process_input(user_input)
            
            # Print the result
            print(result)
        except EOFError:
            # Handle the end of input (Ctrl+D on Unix-like systems)
            break

if __name__ == "__main__":
    main()
    